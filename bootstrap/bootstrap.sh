#!/bin/bash

set -x

apt-get upgrade -y

# Installing docker. Note that this installs the latest version
# from Docker's repo, not the officially supported 18.09 as of
# this writing

sed -i 's/^\/swap/##\/swap/' /etc/fstab
swapoff -a
rm -f /swap.img

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io

# Install the latest kube* packages

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
echo deb https://apt.kubernetes.io/ kubernetes-xenial main > /etc/apt/sources.list.d/kubernetes.list
apt-get update
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl


systemctl enable serial-getty@ttyS0.service
systemctl start serial-getty@ttyS0.service
echo u1 ALL=NOPASSWD: ALL > /etc/sudoers.d/u1
chown -R u1:u1 -R ~u1
