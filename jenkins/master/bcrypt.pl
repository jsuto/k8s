#!/usr/bin/perl
#
# Create a bcrypt password hash
# You need the libcrypt-eksblowfish-perl package
#
use Crypt::Eksblowfish::Bcrypt qw(bcrypt bcrypt_hash en_base64);

die "usage: $0 <password>" unless my $password = $ARGV[0];

my $salt = '';
for my $i (0..15) {
    $salt .= chr(rand(256));
}

my $hash = bcrypt_hash({
    key_nul => 1,
    cost    => 8,
    salt    => $salt,
}, $password);

my $salt_base64 = en_base64($salt);
my $string = bcrypt($password, "\$2a\$08\$$salt_base64");
print $string, "\n";
