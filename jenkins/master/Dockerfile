FROM ubuntu:18.04

LABEL description="jenkins for k8s"

ENV JENKINS_HOME="/var/jenkins"
ENV CASC_JENKINS_CONFIG="/var/jenkins/casc_configs"
ENV JENKINS_UC="https://updates.jenkins.io"
ENV JENKINS_UC_EXPERIMENTAL="https://updates.jenkins.io/experimental"
ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false -Dhudson.DNSMultiCast.disabled=true"

COPY scripts/ /usr/local/bin

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git curl unzip openjdk-8-jre openjdk-8-jdk-headless && \
    mkdir -p /usr/share/jenkins /usr/share/jenkins/ref ${JENKINS_HOME} ${CASC_JENKINS_CONFIG} && \
    curl -L -o /usr/share/jenkins/jenkins.war mirrors.jenkins.io/war-stable/latest/jenkins.war && \
    install-plugins.sh \
       ansicolor \
       authorize-project \
       build-timestamp \
       build-name-setter \
       configuration-as-code \
       email-ext \
       git \
       git-client \
       greenballs \
       job-dsl \
       kubernetes \
       mailer \
       matrix-auth \
       multiple-scms \
       rebuild \
       ssh-agent \
       ssh-credentials \
       timestamper

COPY *.yaml ${CASC_JENKINS_CONFIG}/

CMD ["/usr/local/bin/jenkins.sh"]

EXPOSE 8080 50000
