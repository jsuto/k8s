# Kubernetes monitoring and alerting in less than 5 minutes

This has been tweaked from https://github.com/Thakurvaibhav/k8s/tree/master/monitoring
The original article on medium: https://medium.com/faun/production-grade-kubernetes-monitoring-using-prometheus-78144b835b60

Kubelet natively exposes cadvisor metrics at https://kubernetes.default.svc:443/api/v1/nodes/{node-name}/proxy/metrics/cadvisor
and we can use a prometheus server to scrape this endpoint. These metrics can then be visualized using Grafana. Metrics can also
be scraped from pods and service endpoints if they expose metircs on /metrics (as in the case of nginx-ingress-controller),
alternatively you can sepcify custom scrape target in the prometheus config map. 

Some Important metrics which are not exposed by the kubelet, can be fetched using kube-state-metrics and then pulled by prometheus. 

Setup:

1. If you have not already deployed the nginx-ingress controller then
    - Uncomment `type: LoadBalancer` field in Alertmanager, Prometheus and Grafana Services.

2. Deployment:

kubectl apply -f namespace
kubectl apply -f alertmanager
kubectl apply -f prometheus
kubectl apply -f kube-state-metrics
kubectl apply -f node-exporter
kubectl apply -f grafana

3. Once grafana is running:
- Add DataSource: 
  - Name: DS_PROMETHEUS - Type: Prometheus 
  - URL: http://prometheus-service-ip:8080 
  - Save and Test

You can now build your custon dashboards or simply import dashboards from grafana.net, eg. https://grafana.com/grafana/dashboards/315

Notes:

1. The prometheus config can be reloaded by making an api call to the prometheus server: curl -XPOST http://<prom-service>:<prom-port>/-/reload
2. Please update `00-alertmanager-configmap.yaml` to reflect correct api_url for Slack.
   You can additionally add more receievers. See https://prometheus.io/docs/alerting/configuration/ for more
